//
//  DrawerViewControllerTests.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import XCTest
@testable import WeatherForecast

class DrawerViewControllerTests: XCTestCase {
    
    var controller: DrawerViewController!
    var tableView: UITableView!
//    var dataSource: SearchDataSource!
    var delegate: UITableViewDelegate!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.controller = storyboard.instantiateViewController(withIdentifier: "Drawer") as! DrawerViewController
        
        self.controller.loadView()
        self.controller.viewDidLoad()

        tableView = controller.tableView
        
//        guard let ds = tableView.dataSource as? SearchDataSource else {
//            return XCTFail("Controller's table view should have a SearchDataSource")
//        }
        
//        dataSource = ds
        delegate = tableView.delegate
    }
    
    func testTableViewHasCell() {
        let cell = controller.tableView.dequeueReusableCell(withIdentifier: "CityCell")
        
        XCTAssertNotNil(cell,
                        "TableView should be able to dequeue cell with identifier: 'CityCell'")
    }
    
    func testTableViewDelegateIsDrawerViewController() {
        XCTAssertTrue(tableView.delegate === controller,
                      "Controller should be delegate for the table view")
    }
}
