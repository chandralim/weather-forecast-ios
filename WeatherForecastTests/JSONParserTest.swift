//
//  JSONParserTest.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/29/17.
//  Copyright © 2017 com. All rights reserved.
//

import XCTest
import RealmSwift
import SwiftyJSON
@testable import WeatherForecast
@testable import ISO8601

class JSONParserTest: XCTestCase {
    
    
    let bundle = Bundle(for: JSONParserTest.self)
    
    override func setUp() {
        super.setUp()
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
    }
    
    func testParseCurrentCondition() {
        let json = JSON(data: NSData(contentsOfFile: bundle.path(forResource: "currentCondition", ofType: "json")!)! as Data)
        let currentCondition: CurrentCondition! = CurrentCondition.with(json: json)
        XCTAssertNotNil(currentCondition)
        XCTAssertEqual(currentCondition.identifier, 1503982620)
        XCTAssertEqual(currentCondition.weatherText, "Partly sunny")
        XCTAssertEqual(currentCondition.weatherIcon, 3)
        XCTAssertEqual(currentCondition.temperatures[0].imperials[0].value, 88)
        XCTAssertEqual(currentCondition.temperatures[0].imperials[0].identifier, "F")
        XCTAssertEqual(currentCondition.temperatures[0].imperials[0].unitType, 18)
    }
    
    func testParseForecast() {
        let json = JSON(data: NSData(contentsOfFile: bundle.path(forResource: "forecast", ofType: "json")!)! as Data)
        let dailyForecast: DailyForecast! = DailyForecast.with(json: json)
        XCTAssertNotNil(dailyForecast)
        XCTAssertEqual(dailyForecast.identifier, 1503878400)
        XCTAssertEqual(dailyForecast.date, NSDate(iso8601String: "2017-08-28T07:00:00+07:00") as! Date)
        XCTAssertEqual(dailyForecast.temperatures[0].maximums[0].value, 91)
        XCTAssertEqual(dailyForecast.temperatures[0].minimums[0].value, 74)
        XCTAssertEqual(dailyForecast.nights[0].iconPhrase, "Mostly cloudy")
    }
    
    func testParseCity() {
        let json = JSON(data: NSData(contentsOfFile: bundle.path(forResource: "city", ofType: "json")!)! as Data)
        let city: City! = City.with(json: json)
        XCTAssertNotNil(city)
        XCTAssertEqual(city.identifier, "208971")
        XCTAssertEqual(city.englishName, "Jakarta")
        XCTAssertEqual(city.regions[0].englishName, "Asia")
        XCTAssertEqual(city.countries[0].englishName, "Indonesia")
    }
}
