//
//  City.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class City: Object {
    
    dynamic var identifier: String?
    dynamic var englishName: String?
    open let regions = List<Region>()
    open let countries = List<Country>()
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> City? {
        let identifier = json["Key"].string
        if identifier == nil {
            return nil
        }
        var obj = realm.object(ofType: City.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = City()
            obj?.identifier = identifier
        } else {
            obj = City(value: obj!)
        }
        
        if json["EnglishName"].exists() {
            obj?.englishName = json["EnglishName"].string
        }
        
        if json["Region"].exists() {
            if let region = Region.with(realm: realm, json: json["Region"]) {
                obj?.regions.append(region)
            }
        }
        
        if json["Country"].exists() {
            if let country = Country.with(realm: realm, json: json["Country"]) {
                obj?.countries.append(country)
            }
        }
        return obj
    }
    
    public static func with(json: JSON) -> City? {
        return with(realm: try! Realm(), json: json)
    }
    
}

class Region: Object {
    
    dynamic var identifier: String?
    dynamic var englishName: String?
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Region? {
        let identifier = json["ID"].string
        if identifier == nil {
            return nil
        }
        var obj = realm.object(ofType: Region.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Region()
            obj?.identifier = identifier
        } else {
            obj = Region(value: obj!)
        }
        
        if json["EnglishName"].exists() {
            obj?.englishName = json["EnglishName"].string
        }
        
        return obj
    }
    
    public static func with(json: JSON) -> Region? {
        return with(realm: try! Realm(), json: json)
    }
}

class Country: Object {
    
    dynamic var identifier: String?
    dynamic var englishName: String?
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Country? {
        let identifier = json["ID"].string
        if identifier == nil {
            return nil
        }
        var obj = realm.object(ofType: Country.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Country()
            obj?.identifier = identifier
        } else {
            obj = Country(value: obj!)
        }
        
        if json["EnglishName"].exists() {
            obj?.englishName = json["EnglishName"].string
        }
        
        return obj
    }
    
    public static func with(json: JSON) -> Country? {
        return with(realm: try! Realm(), json: json)
    }
}
