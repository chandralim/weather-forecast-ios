//
//  CurrentCondition.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class CurrentCondition: Object {
    
    dynamic var identifier: Int64 = 0
    dynamic var date: Date?
    dynamic var weatherText: String?
    dynamic var weatherIcon: Int = 0
    open let temperatures = List<Temperature>()
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> CurrentCondition? {
        let identifier = json["EpochTime"].int64Value
        if identifier == 0 {
            return nil
        }
        var obj = realm.object(ofType: CurrentCondition.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = CurrentCondition()
            obj?.identifier = identifier
        } else {
            obj = CurrentCondition(value: obj!)
        }
        if json["LocalObservationDateTime"].exists() {
            obj?.date = DateHelper.iso8601(dateString: json["LocalObservationDateTime"].stringValue)
        }
        if json["WeatherText"].exists() {
            obj?.weatherText = json["WeatherText"].string
        }
        if json["WeatherIcon"].exists() {
            obj?.weatherIcon = json["WeatherIcon"].intValue
        }
        if json["Temperature"].exists() {
            if let temperature = Temperature.with(realm: realm, json: json["Temperature"]) {
                obj?.temperatures.append(temperature)
            }
        }

        return obj
    }
    
    public static func with(json: JSON) -> CurrentCondition? {
        return with(realm: try! Realm(), json: json)
    }
}

class Temperature: Object {
    
    dynamic var identifier: String?
    open let imperials = List<Imperial>()
    open let metrics = List<Metric>()
    open let maximums = List<Maximum>()
    open let minimums = List<Minimum>()
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Temperature? {
        var obj = Temperature()
        
        if json["Imperial"].exists() {
            if let imperial = Imperial.with(realm: realm, json: json["Imperial"]) {
                obj.imperials.append(imperial)
            }
        }
        
        if json["Metric"].exists() {
            if let metric = Metric.with(realm: realm, json: json["Metric"]) {
                obj.metrics.append(metric)
            }
        }
        
        if json["Maximum"].exists() {
            if let maximum = Maximum.with(realm: realm, json: json["Maximum"]) {
                obj.maximums.append(maximum)
            }
        }
        
        if json["Minimum"].exists() {
            if let minimum = Minimum.with(realm: realm, json: json["Minimum"]) {
                obj.minimums.append(minimum)
            }
        }

        return obj
    }
    
    public static func with(json: JSON) -> Temperature? {
        return with(realm: try! Realm(), json: json)
    }
}

class Imperial: Object {
    
    dynamic var identifier: String?
    dynamic var value: Int = 0
    dynamic var unitType: Int = 0
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Imperial? {
        let identifier = json["Unit"].string
        if identifier == nil {
            return nil
        }
        var obj = realm.object(ofType: Imperial.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Imperial()
            obj?.identifier = identifier
        } else {
            obj = Imperial(value: obj!)
        }
        
        if json["Value"].exists() {
            obj?.value = json["Value"].intValue
        }
        if json["UnitType"].exists() {
            obj?.unitType = json["UnitType"].intValue
        }
        
        
        return obj
    }
    
    public static func with(json: JSON) -> Imperial? {
        return with(realm: try! Realm(), json: json)
    }
}

class Metric: Object {
    
    dynamic var identifier: String?
    dynamic var value: Int = 0
    dynamic var unitType: Int = 0
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Metric? {
        let identifier = json["Unit"].string
        if identifier == nil {
            return nil
        }
        var obj = realm.object(ofType: Metric.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Metric()
            obj?.identifier = identifier
        } else {
            obj = Metric(value: obj!)
        }
        
        if json["Value"].exists() {
            obj?.value = json["Value"].intValue
        }
        if json["UnitType"].exists() {
            obj?.unitType = json["UnitType"].intValue
        }
        
        
        return obj
    }
    
    public static func with(json: JSON) -> Metric? {
        return with(realm: try! Realm(), json: json)
    }
}
