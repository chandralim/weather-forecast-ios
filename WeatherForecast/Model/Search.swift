//
//  Search.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class Search: Object {
    
    dynamic var identifier: String?
    dynamic var englishName: String?
    dynamic var country: String?
    dynamic var region: String?
    
    open let cities = List<City>()

}
