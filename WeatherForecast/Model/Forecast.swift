//
//  Forecast.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/29/17.
//  Copyright © 2017 com. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class Forecast: Object {
    
    dynamic var identifier: String?
    open let dailyForecasts = List<DailyForecast>()
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Forecast? {
        let obj = Forecast()

        if json["DailyForecasts"].exists() {
            print("DailyForecasts exists")
            if let dailyForecast = DailyForecast.with(realm: realm, json: json["DailyForecasts"]) {
                obj.dailyForecasts.append(dailyForecast)
            }
        }
        //
        return obj
    }
    
    public static func with(json: JSON) -> Forecast? {
        return with(realm: try! Realm(), json: json)
    }
}

class DailyForecast: Object {
    
    dynamic var identifier: Int64 = 0
    dynamic var date: Date?
    open let temperatures = List<TemperatureForecast>()
    open let days = List<Day>()
    open let nights = List<Night>()
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> DailyForecast? {
        let identifier = json["EpochDate"].int64Value
        if identifier == 0 {
            return nil
        }
        var obj = realm.object(ofType: DailyForecast.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = DailyForecast()
            obj?.identifier = identifier
        } else {
            obj = DailyForecast(value: obj!)
        }
        
        if json["Date"].exists() {
            obj?.date = DateHelper.iso8601(dateString: json["Date"].stringValue)
        }
        if json["Temperature"].exists() {
            if let temperature = TemperatureForecast.with(realm: realm, json: json["Temperature"]) {
                obj?.temperatures.append(temperature)
            }
        }
        if json["Day"].exists() {
            if let day = Day.with(realm: realm, json: json["Day"]) {
                obj?.days.append(day)
            }
        }
        if json["Night"].exists() {
            if let night = Night.with(realm: realm, json: json["Night"]) {
                obj?.nights.append(night)
            }
        }

        
        return obj
    }
    
    public static func with(json: JSON) -> DailyForecast? {
        return with(realm: try! Realm(), json: json)
    }
}

class TemperatureForecast: Object {
    
    dynamic var identifier: Int64 = 0
    open let maximums = List<Maximum>()
    open let minimums = List<Minimum>()
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> TemperatureForecast? {
        
        var identifier = 1
        if identifier == 0 {
            return nil
        }
        var obj = realm.object(ofType: TemperatureForecast.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = TemperatureForecast()
            obj?.identifier = Int64(identifier)
            identifier += 1
        } else {
            obj = TemperatureForecast(value: obj!)
        }

        if json["Maximum"].exists() {
            if let maximum = Maximum.with(realm: realm, json: json["Maximum"]) {
                obj?.maximums.append(maximum)
            }
        }
        
        if json["Minimum"].exists() {
            if let minimum = Minimum.with(realm: realm, json: json["Minimum"]) {
                obj?.minimums.append(minimum)
            }
        }
        return obj
    }
    
    public static func with(json: JSON) -> TemperatureForecast? {
        return with(realm: try! Realm(), json: json)
    }
}

class Maximum: Object {
    
    dynamic var identifier: String?
    dynamic var value: Int = 0
    dynamic var unitType: Int = 0
    dynamic var valueCelcius: Int = 0
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Maximum? {
        let identifier =
            PreferenceManager.base64(from: "\(json["Unit"].string)::\(String(json["Value"].intValue))::\(json["UnitType"].string)")
        
        if identifier == nil {
            return nil
        }
        var obj = realm.object(ofType: Maximum.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Maximum()
            obj?.identifier = String(describing: identifier)
        } else {
            obj = Maximum(value: obj!)
        }
        
        if json["Value"].exists() {
            obj?.value = json["Value"].intValue
        }
        if json["UnitType"].exists() {
            obj?.unitType = json["UnitType"].intValue
        }
        obj?.valueCelcius = ((obj?.value)! - 32) * 5 / 9
        
        
        return obj
    }
    
    public static func with(json: JSON) -> Maximum? {
        return with(realm: try! Realm(), json: json)
    }
}

class Minimum: Object {
    
    dynamic var identifier: String?
    dynamic var value: Int = 0
    dynamic var unitType: Int = 0
    dynamic var valueCelcius: Int = 0
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Minimum? {
        let identifier =             PreferenceManager.base64(from: "\(json["Unit"].string)::\(String(json["Value"].intValue))::\(json["UnitType"].string)")

        if identifier == nil {
            return nil
        }
        var obj = realm.object(ofType: Minimum.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Minimum()
            obj?.identifier = String(describing: identifier)
        } else {
            obj = Minimum(value: obj!)
        }
        
        if json["Value"].exists() {
            obj?.value = json["Value"].intValue
        }
        if json["UnitType"].exists() {
            obj?.unitType = json["UnitType"].intValue
        }
        obj?.valueCelcius = ((obj?.value)! - 32) * 5 / 9
        
        
        return obj
    }
    
    public static func with(json: JSON) -> Minimum? {
        return with(realm: try! Realm(), json: json)
    }
}

class Day: Object {
    
    dynamic var identifier: Int64 = 0
    dynamic var iconPhrase: String?
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Day? {
        let identifier = json["Icon"].int64Value
        
        if identifier == 0 {
            return nil
        }
        var obj = realm.object(ofType: Day.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Day()
            obj?.identifier = identifier
        } else {
            obj = Day(value: obj!)
        }
        
        if json["IconPhrase"].exists() {
            obj?.iconPhrase = json["IconPhrase"].string
        }

        return obj
    }
    
    public static func with(json: JSON) -> Day? {
        return with(realm: try! Realm(), json: json)
    }
}

class Night: Object {
    
    dynamic var identifier: Int64 = 0
    dynamic var iconPhrase: String?
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> Night? {
        let identifier = json["Icon"].int64Value
        
        if identifier == 0 {
            return nil
        }
        var obj = realm.object(ofType: Night.self, forPrimaryKey: identifier)
        if obj == nil {
            obj = Night()
            obj?.identifier = identifier
        } else {
            obj = Night(value: obj!)
        }
        
        if json["IconPhrase"].exists() {
            obj?.iconPhrase = json["IconPhrase"].string
        }
        
        return obj
    }
    
    public static func with(json: JSON) -> Night? {
        return with(realm: try! Realm(), json: json)
    }
}
