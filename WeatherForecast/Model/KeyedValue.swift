//
//  KeyedValue.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

open class KeyedValue: Object {
    
    dynamic open var key: String = ""
    dynamic open var value: String?
    
    override open static func primaryKey() -> String? {
        return "key"
    }
    
}
