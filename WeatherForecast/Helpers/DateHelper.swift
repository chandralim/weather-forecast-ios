//
//  DateHelper.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/29/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit

open class DateHelper: NSObject {
    
    public static func iso8601(dateString: String) -> Date? {
        let dateFormatter = DateFormatter()
        //        dateFormatter.locale = Locale(identifier: "id_ID")
        //        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateString)
    }
    
    public static func date(_ dateString: String, withFormatString formatString: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatString
        return dateFormatter.date(from: dateString)
    }
    
}
