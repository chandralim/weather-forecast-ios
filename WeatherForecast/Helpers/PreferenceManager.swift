//
//  PreferenceManager.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/29/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit

class PreferenceManager: NSObject {
    
    static let instance = PreferenceManager()
    
    static func base64(from string: String) -> String? {
        let data = (string).data(using: String.Encoding.utf8)
        if let data = data {
            let base64 = data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            return base64
        } else {
            return nil
        }
    }
}
