//
//  UIViewController+Additions.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit
import REFrostedViewController

extension UIViewController {
    
    func setupNavigationDrawer() {
        let barButtonItemMenu = UIBarButtonItem(image: UIImage(named: "ic_menu"), style: .plain, target: self, action: #selector(UIViewController.menuPressed(_:)))
        barButtonItemMenu.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButtonItemMenu
    }
    
    func setupLeftBarButtonItems(menu: Bool, close: Bool, back: Bool = false, white: Bool = false) {
        var items = [UIBarButtonItem]()
        if menu {
            let barButtonItemMenu = UIBarButtonItem(image: UIImage(named: "ic_menu"), style: .plain, target: self, action: #selector(UIViewController.menuPressed(_:)))
            barButtonItemMenu.tintColor = white ? UIColor.white : UIColor(hex: 0x232323)
            items.append(barButtonItemMenu)
        }
        if close {
            var barButtonItemClose = UIBarButtonItem(image: UIImage(named: "ic_close_white"), style: .plain, target: self, action: #selector(UIViewController.closePressed(_:)))
            if back {
                barButtonItemClose = UIBarButtonItem(image: UIImage(named: "ic_back_black"), style: .plain, target: self, action: #selector(UIViewController.backPressed(_:)))
            }
            barButtonItemClose.tintColor = white ? UIColor.white : UIColor(hex: 0x232323)
            items.append(barButtonItemClose)
        }
        navigationItem.leftBarButtonItems = items
    }
    
    func setupBackBarButtonItems(back: Bool, title: String? = "") {
        var items = [UIBarButtonItem]()
        if back {
            let barButtonItemMenu = UIBarButtonItem(image: UIImage(named: "ic_back_black"), style: .plain, target: self, action: #selector((backPressed)))
            barButtonItemMenu.tintColor = UIColor(hex: 0x232323)
            items.append(barButtonItemMenu)
        }
        if let title = title, title != "" {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
            label.textColor = UIColor.white
            label.textAlignment = .left
            label.text = title
            label.font = UIFont.systemFont(ofSize: 16)
            let barButtonItemMenu = UIBarButtonItem(customView: label)
            items.append(barButtonItemMenu)
        }
        navigationItem.leftBarButtonItems = items
    }
    
    func setupPanGesture() {
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(UIViewController.panGestureRecognized(_:))))
    }
    
    func menuPressed(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.frostedViewController?.view.endEditing(true)
        self.frostedViewController?.presentMenuViewController()
    }
    
    func closePressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func panGestureRecognized(_ recognizer: UIPanGestureRecognizer) {
        view.endEditing(true)
        frostedViewController?.view.endEditing(true)
        frostedViewController?.panGestureRecognized(recognizer)
    }
    
    func setupTitle(_ title: String? = "title") {
        //var items = [UIBarButtonItem]()
        if let title = title {
            //            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
            //            label.textColor = white ? UIColor.white : UIColor(hex: 0x232323)
            //            label.textAlignment = .left
            //            label.text = title
            //            label.font = UIFont.systemFont(ofSize: 17)
            //            let barButtonItemMenu = UIBarButtonItem(customView: label)
            //            items.append(barButtonItemMenu)
            navigationItem.title = title
        }
        //navigationItem .leftBarButtonItems = items
    }
    
    func setupStatusBarTintByNavController() {
        if let navigationController = navigationController {
            navigationController.navigationBar.barStyle = UIBarStyle.default
        }
    }
    
    func backPressed(_ sender: UIBarButtonItem) {
        if isModal() {
            self.dismiss(animated: true, completion: nil)
        } else {
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func isModal() -> Bool {
        if self.navigationController?.viewControllers.count == 1 {
            return true
        }
        return false
    }
    
    func imageLayerForGradientBackground() -> UIImage {
        
        var updatedFrame = self.navigationController?.navigationBar.bounds
        // take into account the status bar
        updatedFrame?.size.height += 20
        let layer = self.setGradientLayerForBounds(firstColor: UIColor(hex: 0xffffff), secondColor: UIColor(hex: 0xffffff),bounds: updatedFrame!)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func setGradientLayerForBounds(firstColor: UIColor, secondColor: UIColor, bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        return layer
    }
    
}

