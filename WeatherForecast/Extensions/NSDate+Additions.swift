//
//  NSDate+Additions.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/29/17.
//  Copyright © 2017 com. All rights reserved.
//

import Foundation

extension Date {
    
    func dateWithoutTime() -> Date {
        let components = (Calendar.current as NSCalendar).components([.year, .month, .day], from: self)
        return Calendar.current.date(from: components)!
    }
    
    func dateByAddingDays(_ days: Int) -> Date {
        return (Calendar.current as NSCalendar).date(
            byAdding: .day,
            value: days,
            to: self,
            options: [])!
    }
    
    func toString(_ format:String? = "dd MMMM yyyy") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func timeAgo(_ numericDates:Bool) -> String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        
        let now = Date()
        
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([NSCalendar.Unit.year, .month, .weekOfMonth, .day, .hour, .minute, .second],
                                                             from: self,
                                                             to: now,
                                                             options:NSCalendar.Options(rawValue: 0))
        
        if components.year! > 0 {
            formatter.allowedUnits = .year
        } else if components.month! > 0 {
            formatter.allowedUnits = .month
        } else if components.weekOfMonth! > 0 {
            formatter.allowedUnits = .weekOfMonth
        } else if components.day! > 0 {
            formatter.allowedUnits = .day
        } else if components.hour! > 0 {
            formatter.allowedUnits = .hour
        } else if components.minute! > 0 {
            formatter.allowedUnits = .minute
        } else {
            formatter.allowedUnits = .second
        }
        
        let formatString = NSLocalizedString("%@ ago", comment: "Used to say how much time has passed. e.g. '2 hours ago'")
        
        guard let timeString = formatter.string(from: components) else {
            return ""
        }
        return String(format: formatString, timeString)
        
    }
    
    func timeDiffToNow() -> DateComponents? {
        let now = Date()
        let calendar = Calendar.current
        let diff = calendar.dateComponents([.day, .hour, .minute, .second], from: now, to: self)
        return diff
    }
    
}
