//
//  WeatherForecast-bridging-header.h
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

#ifndef WeatherForecast_bridging_header_h
#define WeatherForecast_bridging_header_h

#import <UIKit/UIKit.h>

#import "MXSegmentedPager.h"
#import "HMSegmentedControl.h"
#import "MXPagerView.h"
#import "MXPagerViewController.h"
#import "MXSegmentedPagerController.h"

#endif /* WeatherForecast_bridging_header_h */
