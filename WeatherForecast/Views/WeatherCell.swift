//
//  WeatherCell.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/29/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit

class WeatherCell: UICollectionViewCell {
    
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelToday: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var labelHigh: UILabel!
    @IBOutlet weak var labelLow: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
