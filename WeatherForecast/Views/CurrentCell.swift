//
//  CurrentCell.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit

class CurrentCell: UITableViewCell {

    @IBOutlet weak var labelDegree: UILabel!
    @IBOutlet weak var labelCityName: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var labelStatus: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var currentCondition: CurrentCondition? {
        didSet {
            if let currentCondition = currentCondition {
//                labelCityName.text = currentCondition.
                labelStatus.text = currentCondition.weatherText
//                labelDegree.text = String(currentCondition.temperatures[0].metrics[0].value)
            }
        }
    }

}
