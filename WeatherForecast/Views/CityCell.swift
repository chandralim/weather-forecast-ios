//
//  CityCell.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {
    
    @IBOutlet weak var labelCountry: UILabel!
    @IBOutlet weak var labelCityName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var city: City? {
        didSet {
            if let city = city {
                if let name = city.englishName, let countries = city.countries[0].englishName, let regions = city.regions[0].englishName {
                    labelCityName.text = "\(name)"
                    
                    labelCountry.text = "\(countries), \(regions)"
                }
            }
        }
    }

}
