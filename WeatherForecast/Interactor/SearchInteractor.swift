//
//  SearchInteractor.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class SearchInteractor: BaseInteractor {
    
    open var searches:[Search] = [Search]()
    
    open func loadSearch() {
        let result = realm.objects(Search.self)
        if result.count > 0 {
            searches.removeAll()
            for search in result {
                let search = Search(value: search)
                searches.append(search)
            }
        }
    }
}
