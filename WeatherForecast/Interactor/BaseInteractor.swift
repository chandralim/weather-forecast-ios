//
//  BaseInteractor.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

open class BaseInteractor {
    
    public var service = WeatherCoreService.instance
    
    public var perPage: Int
    public var params: [String: String]
    public let storeKey: String?
    public var lastPage = 0
    public var nextPage = 1
    
    private var _realm: Realm?
    
    public var realm: Realm {
        if let realm = _realm {
            return realm
        }
        return try! Realm()
    }
    
    public init(perPage: Int = 10, params: [String: String] = [String: String](), storeKey: String? = nil) {
        self.perPage = perPage
        self.params = params
        self.storeKey = storeKey
    }
    
    //    public func load(realmType: Object) -> Object {
    //        var items = [Object]()
    //        if let key = storeKey,
    //            let keyedValue = realm.object(ofType: KeyedValue.self, forPrimaryKey: key as AnyObject),
    //            let value = keyedValue.value
    //        {
    //            let json = JSON.parse(value)
    //            for child in json.arrayValue {
    //                if let item = realm.objectForPrimaryKey(realmType.self, key: child.rawValue) {
    //                    items.append(item)
    //                }
    //            }
    //        }
    //    }
    
    public func saveModel(data: Object) {
        try! self.realm.write {
            self.realm.add(data, update: true)
        }
        if let key = storeKey {
            var separatedData = [AnyObject]()
            separatedData.append(data as AnyObject)
            let json = JSON(separatedData)
            let string = json.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))
            try! realm.write {
                let keyedValue = KeyedValue()
                keyedValue.key = key
                keyedValue.value = string
                self.realm.create(KeyedValue.self, value: keyedValue, update: true)
            }
        }
    }
    
    public func saveListOfModels(data: [Object]) {
        try! self.realm.write {
            self.realm.add(data, update: true)
        }
        if let key = storeKey {
            var separatedData = [AnyObject]()
            for item in data {
                if let value = item.value(forKey: "identifier") {
                    separatedData.append(value as AnyObject)
                }
            }
            let json = JSON(separatedData)
            let string = json.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))
            try! realm.write {
                let keyedValue = KeyedValue()
                keyedValue.key = key
                keyedValue.value = string
                self.realm.create(KeyedValue.self, value: keyedValue, update: true)
            }
        }
    }
    
    public func removeAllModelsOf<T : RealmSwift.Object>(type: T.Type, filter: String? = nil) {
        if let filter = filter {
            let result = self.realm.objects(type).filter(filter)
            try! self.realm.write {
                self.realm.delete(result)
                print("\(type.description()) deleted, filter : \(filter)")
            }
        } else {
            let result = self.realm.objects(type)
            try! self.realm.write {
                self.realm.delete(result)
                print("\(type.description()) deleted")
            }
        }
    }
    
}
