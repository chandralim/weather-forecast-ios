//
//  CityInteractor.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class CityInteractor: BaseInteractor {
    
    fileprivate var currentPage = 1
    open var hasNext: Bool {
        return currentPage != -1
    }
    
    var items: [City] = [City]()
    
    func loadKey() {
        items.removeAll()
        if let key = storeKey,
            let keyedValue = realm.object(ofType: KeyedValue.self, forPrimaryKey: key as AnyObject),
            let value = keyedValue.value
        {
            let json = JSON.parse(value)
            for child in json.arrayValue {
                if let item = realm.object(ofType: City.self, forPrimaryKey: child.rawValue as AnyObject) {
                    items.append(City(value: item))
                }
            }
        }
    }
    
    func refresh(success: @escaping () -> (Void), failure: @escaping (NSError) -> (Void)) {
        currentPage = 1
        nextWith(success: success, failure: failure)
    }
    
    func nextWith(success: @escaping () -> (Void), failure: @escaping (NSError) -> (Void)) {
        service.getCities(params: params, page: currentPage, perPage: perPage, callback: { result in
            switch result {
            case let .Success(pagination) :
                if pagination.currentPage == 1 {
                    self.items.removeAll()
                }
                if pagination.currentPage >= pagination.lastPage {
                    self.currentPage = -1
                } else {
                    self.currentPage = pagination.currentPage + 1
                }
                var dataForSaved = [City]()
                for object in pagination.data {
                    dataForSaved.append(City(value: object))
                }
                self.saveListOfModels(data: dataForSaved)
                self.items.append(contentsOf: pagination.data)
                success()
            case let .Failure(error) :
                failure(error)
            }
        })
    }
}
