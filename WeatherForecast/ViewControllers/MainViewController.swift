//
//  MainViewController.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit
import REFrostedViewController

class MainViewController: REFrostedViewController {
    
    static func instantiate() -> MainViewController {
        return UIStoryboard.main.instantiateViewController(withIdentifier: "Main") as! MainViewController
    }
    
    override func awakeFromNib() {
        menuViewController = DrawerViewController.instantiate()
        contentViewController = HomeViewController.instantiateNav(idCity: "initial", nameCity: "")
        panGestureEnabled = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let menuSize = 240
        menuViewSize = CGSize(width: menuSize, height: 0)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
