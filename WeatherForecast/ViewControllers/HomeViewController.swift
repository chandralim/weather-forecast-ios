//
//  HomeViewController.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit
import SVProgressHUD
import SVPullToRefresh
import DropDown
import RealmSwift
import AlamofireImage
import DZNEmptyDataSet

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelStatusWeather: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var labelCityName: UILabel!
    @IBOutlet weak var labelDegree: NSLayoutConstraint!
    @IBOutlet weak var buttonDegree: UIButton!
    @IBOutlet weak var buttonFar: UIButton!
    
    var celciusChecked = true
    
    var idCity: String? = ""
    var nameCity: String? = ""
    var shouldShowCurrentWeather = false
    var shouldShowForecastWeather = false
    
    let interactor: CurrentConditionInteractor? = CurrentConditionInteractor(params:[
        "apikey" : "t6mGze0ugafjqTIif3GX0NLPPmRcdvVh"])
    
    let interactorForecast: ForecastInteractor? = ForecastInteractor(params:[
        "apikey" : "t6mGze0ugafjqTIif3GX0NLPPmRcdvVh"])
    
    var refreshed = false
    
    static func instantiateNav(idCity: String?, nameCity: String?) -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
        let controller = nav.topViewController as! HomeViewController
        controller.idCity = idCity
        controller.nameCity = nameCity
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStatusBarTintByNavController()
        
        setupLeftBarButtonItems(menu: true, close: false)
        setupPanGesture()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        tableView.register(UINib(nibName: "CurrentCell", bundle: nil), forCellReuseIdentifier: "CurrentCell")
        tableView.register(UINib(nibName: "ForecastCell", bundle: nil), forCellReuseIdentifier: "ForecastCell")
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        if idCity != "initial" {
            print("idCity = \(idCity)")
            tableView.addPullToRefresh {
                self.refresh()
            }
            tableView.triggerPullToRefresh()
            tableView.reloadData()
        }
        
        buttonDegree.backgroundColor = UIColor.blue
        buttonDegree.setTitleColor(UIColor.white, for: .normal)
        
        buttonFar.backgroundColor = UIColor.white
        buttonFar.setTitleColor(UIColor.blue, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    
    func refresh() {
        if let idCity = idCity {
            interactor?.refresh(idCity: idCity, success: {
                self.refreshed = true
                self.tableView.pullToRefreshView.stopAnimating()
                self.shouldShowCurrentWeather = true
                self.tableView.reloadData()
            }, failure: { error in
                print(error.localizedDescription)
                self.tableView.pullToRefreshView.stopAnimating()
            })
            
            interactorForecast?.refresh(idCity: idCity, success: {
                self.refreshed = true
                self.tableView.pullToRefreshView.stopAnimating()
                self.shouldShowForecastWeather = true
                self.tableView.reloadData()
            }, failure: { error in
                print(error.localizedDescription)
                self.tableView.pullToRefreshView.stopAnimating()
            })
        }
    }
    
    @IBAction func changeDegree(_ sender: Any) {
        celciusChecked = true
        
        tableView.reloadData()
        buttonDegree.backgroundColor = UIColor.blue
        buttonDegree.setTitleColor(UIColor.white, for: .normal)
        
        buttonFar.backgroundColor = UIColor.white
        buttonFar.setTitleColor(UIColor.blue, for: .normal)
        
    }
    
    @IBAction func buttonFPressed(_ sender: Any) {
        celciusChecked = false

        tableView.reloadData()
        
        buttonFar.backgroundColor = UIColor.blue
        buttonFar.setTitleColor(UIColor.white, for: .normal)
        
        buttonDegree.backgroundColor = UIColor.white
        buttonDegree.setTitleColor(UIColor.blue, for: .normal)
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
           return 170
        } else {
            return 50
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowCurrentWeather && shouldShowForecastWeather {
            if section == 0 {
                return 1
            } else {
                return 5
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if shouldShowCurrentWeather {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentCell", for: indexPath) as! CurrentCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                if let interactor = interactor?.items[0] {
                    cell.labelStatus.text = interactor.weatherText
                    if let nameCity = nameCity, let date = interactor.date {
                        cell.labelCityName.text = "\(nameCity)\n\(date.toString("dd MMMM yyyy HH:mm:ss"))"
                    }
                    
                    if celciusChecked == true {
                        cell.labelDegree.text = String(interactor.temperatures[0].metrics[0].value)
                    } else {
                        cell.labelDegree.text = String(interactor.temperatures[0].imperials[0].value)
                    }
                    
                    var urlString: String?
                    urlString = "https://developer.accuweather.com/sites/default/files/\(interactor.weatherIcon)-s.png"
                    
                    if let urlString = urlString, let url = URL(string: urlString) {
                        cell.imageWeather.af_setImage(withURL: url, placeholderImage: UIImage(named: "placeholder"), imageTransition: .flipFromTop(0.4))
                    } else {
                        cell.imageWeather.image = UIImage(named: "placeholder")
                    }
                    return cell
                }
            }
        }
        
        if shouldShowForecastWeather {
            if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell", for: indexPath) as! ForecastCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                if let interactor = interactorForecast?.items[indexPath.row] {
                    cell.labelDay.text = interactor.date?.toString()
                    
                    if celciusChecked == true {
                        cell.labelMax.text = String(interactor.temperatures[0].maximums[0].valueCelcius)
                        cell.labelMin.text = String(interactor.temperatures[0].minimums[0].valueCelcius)
                    } else {
                        cell.labelMax.text = String(describing: interactor.temperatures[0].maximums[0].value)
                        cell.labelMin.text = String(describing: interactor.temperatures[0].minimums[0].value)
                    }
                    
                    var urlString: String?
                    urlString = "https://developer.accuweather.com/sites/default/files/\(interactor.nights[0].identifier)-s.png"
                    
                    if let urlString = urlString, let url = URL(string: urlString) {
                        cell.imageWeather.af_setImage(withURL: url, placeholderImage: UIImage(named: "placeholder"), imageTransition: .flipFromTop(0.4))
                    } else {
                        cell.imageWeather.image = UIImage(named: "placeholder")
                    }
                }
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
    }
}

extension HomeViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Pick one city to display."
        let attrib = [NSForegroundColorAttributeName: UIColor.lightGray]
        let attribString = NSAttributedString(string: text, attributes: attrib)
        return attribString
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -25
    }
}
