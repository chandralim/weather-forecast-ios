//
//  SearchCityViewController.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import RealmSwift
import SVProgressHUD

class SearchCityViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var searchKey: String?
    
    var withClose = false
    var titleNav = ""
    var shouldShowSearchResults = false
    
    var interactor: CityInteractor? = CityInteractor()
    var refreshed = false
    
    let realm = try! Realm()
    
    static func instantiateNav(withClose: Bool = false, titleNav: String = "") -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "SearchNav") as! UINavigationController
        let controller = nav.topViewController as! SearchCityViewController
        controller.withClose = withClose
        controller.titleNav = titleNav
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStatusBarTintByNavController()
        
        setupTitle(titleNav)
        setupLeftBarButtonItems(menu: !withClose, close: withClose, back: withClose)
        
        searchBar.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "CityCell", bundle: nil), forCellReuseIdentifier: "CityCell")
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.reloadData()
    }
}

extension SearchCityViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchKey = searchText
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        interactor = CityInteractor(params:[
            "apikey" : "t6mGze0ugafjqTIif3GX0NLPPmRcdvVh",
            "q" : searchKey!])
        
        SVProgressHUD.show(withStatus: "Searching . . .")
        self.interactor?.refresh(success: {
            self.tableView.showsInfiniteScrolling = (self.interactor?.hasNext)!
            self.shouldShowSearchResults = true
            self.tableView.reloadData()
            
            if (self.interactor?.items.count)! > 0 {
               SVProgressHUD.dismiss()
            } else {
                SVProgressHUD.showInfo(withStatus: "No city match")
            }
        }, failure: { error in
            print(error.localizedDescription)
            self.tableView.pullToRefreshView.stopAnimating()
        })
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchKey = ""
        tableView.reloadData()
        searchBar.resignFirstResponder()
        self.shouldShowSearchResults = false
    }
}

extension SearchCityViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return (interactor?.items.count)!
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as? CityCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        if shouldShowSearchResults {
            if let interactor = interactor?.items[indexPath.row] {
                cell?.city = interactor
            }
            
        } else {
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var realm: Realm?
        do {
            realm = try Realm()
        } catch {
            print("error")
        }
        
        do {
            if let realm = realm {
                try realm.write {
                    let search = Search()
                    search.identifier = interactor?.items[indexPath.row].identifier
                    search.englishName = interactor?.items[indexPath.row].englishName
                    search.country = interactor?.items[indexPath.row].countries[0].englishName
                    search.region = interactor?.items[indexPath.row].regions[0].englishName
                    realm.add(search)

                    self.dismiss(animated: true, completion: nil)
                }
            }
        } catch {
            print("error")
        }
    }
}

extension SearchCityViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Search some city"
        let attrib = [NSForegroundColorAttributeName: UIColor.lightGray]
        let attribString = NSAttributedString(string: text, attributes: attrib)
        return attribString
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -25
    }
}
