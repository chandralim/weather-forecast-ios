//
//  DrawerViewController.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit
import SVProgressHUD
import SVPullToRefresh
import RealmSwift
import DZNEmptyDataSet

class DrawerViewController: UIViewController {
    
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    let interactor = SearchInteractor()
    
    static func instantiate() -> DrawerViewController {
        return UIStoryboard.main.instantiateViewController(withIdentifier: "Drawer") as! DrawerViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        tableView.register(UINib(nibName: "CityCell", bundle: nil), forCellReuseIdentifier: "CityCell")
        
        let result = realm.objects(Search.self)
        if result.count > 0 {
            interactor.loadSearch()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.loadSearch()
        tableView.reloadData()
    }
    
    @IBAction func buttonAddPressed(_ sender: UIButton) {
        let nav = SearchCityViewController.instantiateNav(withClose: true, titleNav: "Search City")
        self.present(nav, animated: true, completion: nil)
    }
}

extension DrawerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let search = realm.objects(Search.self)
        if search.count > 0 {
            return search.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as? CityCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        let search = interactor.searches[indexPath.row]
        
        if let englishName = search.englishName {
            cell?.labelCityName.text = englishName
        }
        if let country = search.country, let region = search.region {
            cell?.labelCountry.text = "\(country), \(region)"
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = interactor.searches[indexPath.row].identifier, let name = interactor.searches[indexPath.row].englishName {
            frostedViewController.contentViewController = HomeViewController.instantiateNav(idCity: id, nameCity: name)
            
            frostedViewController.hideMenuViewController()
        }
    }
}

extension DrawerViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "No added city yet.\nWhy not add some."
        let attrib = [NSForegroundColorAttributeName: UIColor.lightGray]
        let attribString = NSAttributedString(string: text, attributes: attrib)
        return attribString
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -25
    }
}
