//
//  WeatherCore.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import Foundation
import RealmSwift

public class WeatherCore: NSObject {
    public static func setup(home: String) {
        var config = Realm.Configuration(
            schemaVersion: 3,
            migrationBlock: { (migration, oldSchemaVersion) in
                if (oldSchemaVersion < 3) {
                    print("<<REALM: PROVIDED SCHEMA VERSION IS LESS THAN LAST SET VERSION>>")
                }
        }, deleteRealmIfMigrationNeeded: true, objectTypes: nil)
        Realm.Configuration.defaultConfiguration = config
        let _ = try! Realm()
        if let url = config.fileURL {
            print(url.absoluteString)
        }
        WeatherCoreService.instance.home = home
    }
}
