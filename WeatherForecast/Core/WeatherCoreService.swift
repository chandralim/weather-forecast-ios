//
//  WeatherCoreService.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public struct Pagination<T> {
    public let total: Int
    public let currentPage: Int
    public let lastPage: Int
    public let data: T
    
    public init(total: Int, currentPage: Int, lastPage: Int, data: T) {
        self.total = total
        self.currentPage = currentPage
        self.lastPage = lastPage
        self.data = data
    }
}

public enum APIResult<T> {
    case Success(T)
    case Failure(NSError)
}

open class WeatherCoreService: NSObject {
    public static let instance = WeatherCoreService()
    
    public let manager = APIManager()
    public var home = ""
    
    public func setup(home: String) {
        self.home = home
    }
}

public class APIManager: SessionManager {
    
    override public func request(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> DataRequest {
        do {
            let urlRequest = URLRequest(url: try url.asURL())
            let request = try! encoding.encode(urlRequest, with: parameters)
            print("[\(method.rawValue)] \(request)")
        } catch {
            print("Error URL Request")
        }
        return super.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
}

extension Request {
    
    func log() -> Request {
        print("[\(self.request!.httpMethod!)] \(self.request!.url)")
        return self
    }
}
