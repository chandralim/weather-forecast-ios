//
//  WeatherCoreExtensions.swift
//  WeatherForecast
//
//  Created by Chandra Welim on 8/28/17.
//  Copyright © 2017 com. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

extension WeatherCoreService {
    
    static let API_KEY = "t6mGze0ugafjqTIif3GX0NLPPmRcdvVh"
    
    func getCities(params: [String : String], page: Int = 1, perPage: Int = 10, callback: @escaping (APIResult<Pagination<[City]>>) -> (Void)) {
        var parameters: [String: AnyObject] = [:]
        for (key, value) in params {
            parameters[key] = value as AnyObject?
        }
    
        let request = manager.request(home + "/locations/v1/cities/search", method: .get, parameters: parameters)
        request.responseJSON { response in
            switch response.result {
            case let .success(value):
                let json = JSON(value)

                let total = perPage
                let currentPage = page
                let lastPage = currentPage
            
                var items = [City]()
                for itemJson in json.arrayValue {
                    if let item = City.with(json: itemJson) {
                        items.append(item)
                    }
                }
                let pagination = Pagination<[City]>(total: total, currentPage: currentPage, lastPage: lastPage, data: items)
                callback(.Success(pagination))
          
            case let .failure(error):
                print(error.localizedDescription)
                callback(.Failure(error as NSError))
            }
        }
    }
    
    func getCurrentConditions(idCity: String, params: [String : String], page: Int = 1, perPage: Int = 10, callback: @escaping (APIResult<Pagination<[CurrentCondition]>>) -> (Void)) {
        var parameters: [String: AnyObject] = [:]
        for (key, value) in params {
            parameters[key] = value as AnyObject?
        }
        
        let request = manager.request(home + "/currentconditions/v1/\(idCity)", method: .get, parameters: parameters)
        request.responseJSON { response in
            switch response.result {
            case let .success(value):
                let json = JSON(value)
                
                let total = perPage
                let currentPage = page
                let lastPage = currentPage
                
                var items = [CurrentCondition]()
                for itemJson in json.arrayValue {
                    if let item = CurrentCondition.with(json: itemJson) {
                        items.append(item)
                    }
                }
                let pagination = Pagination<[CurrentCondition]>(total: total, currentPage: currentPage, lastPage: lastPage, data: items)
                callback(.Success(pagination))
                
            case let .failure(error):
                print(error.localizedDescription)
                callback(.Failure(error as NSError))
            }
        }
    }
    
    func getForecasts(idCity: String, params: [String : String], page: Int = 1, perPage: Int = 10, callback: @escaping (APIResult<Pagination<[DailyForecast]>>) -> (Void)) {
        var parameters: [String: AnyObject] = [:]
        for (key, value) in params {
            parameters[key] = value as AnyObject?
        }
        
        let request = manager.request(home + "/forecasts/v1/daily/5day/\(idCity)", method: .get, parameters: parameters)
        request.responseJSON { response in
            switch response.result {
            case let .success(value):
                let json = JSON(value)
                let total = perPage
                let currentPage = page
                let lastPage = currentPage
                
                var items = [DailyForecast]()
                for itemJson in json["DailyForecasts"].arrayValue {
                    if let item = DailyForecast.with(json: itemJson) {
                        items.append(item)
                    }
                }
                let pagination = Pagination<[DailyForecast]>(total: total, currentPage: currentPage, lastPage: lastPage, data: items)
//                print("pagination \(pagination)")
                callback(.Success(pagination))
                
            case let .failure(error):
                print(error.localizedDescription)
                callback(.Failure(error as NSError))
            }
        }
    }
}

public class SessionManagerWeather: APIManager {
    
    override public func request(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> DataRequest {
        var overidedParameters = [String: AnyObject]()
        overidedParameters["apikey"] = WeatherCoreService.API_KEY as AnyObject
        if let parameters = parameters {
            for (key, value) in parameters {
                overidedParameters[key] = value as AnyObject?
            }
        }
        return super.request(url, method: method, parameters: overidedParameters, encoding: encoding, headers: headers)
    }
    
}
